﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Kenshyn.Innovamat
{
    public class StatsMenu : MonoBehaviour
    {
        public Text FirstTimeSuccess, SecondTimeSuccess, FirstTimeFailure, SecondTimeFailure;


        private void Start()
        {
            LoadText();
        }

        private void LoadText()
        {

            SaveData data = SaveLoadManager.Instance.LoadData();

            FirstTimeSuccess.text = "Veces que se ha acertado a la primera: " + data.numberOfFirstTimeSucces.ToString();
            SecondTimeSuccess.text = "Veces que se ha acertado a la segunda: " + data.numberOfSecondTimeSucces.ToString();
            FirstTimeFailure.text = "Veces que se ha fallado a la primera: " + data.numberOfFirstTimeFailure.ToString();
            SecondTimeFailure.text = "Veces que se ha acertado a la segunda " + data.numberOfSecondTimeFailure.ToString();
        }

        public void ResetStats()
        {
            SaveLoadManager.Instance.ResetData();
            LoadText();
        }

        public void Exit()
        {
            GameManager.Instance.LoadLevel("MainMenu");
        }
    }
}