﻿using UnityEngine;


/// <summary>
/// Singleton pattern.
/// </summary>
public class PersistentSingleton<T> : Singleton<T> where T : Component
{
    /// <summary>
    /// On awake, we initialize our instance. Make sure to call base.Awake() in override if you need awake.
    /// </summary>
    protected override void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else DontDestroyOnLoad(this);
        base.Awake();
    }
}

