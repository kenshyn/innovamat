﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kenshyn.Innovamat
{
    public class MainMenu : MonoBehaviour
    {
        /// <summary>
        /// Loads a different level
        /// </summary>
        /// <param name="level"></param>
        public void LoadLevel(string level)
        {
            GameManager.Instance.LoadLevel(level);
        }

        /// <summary>
        /// Quits the game
        /// </summary>
        public void ExitGame()
        {
            Application.Quit();
        }

    }
}