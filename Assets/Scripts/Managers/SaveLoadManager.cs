﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Kenshyn.Innovamat
{
    [System.Serializable]
    public struct SaveData
    {
        public int numberOfFirstTimeSucces;
        public int numberOfSecondTimeSucces;
        public int numberOfFirstTimeFailure;
        public int numberOfSecondTimeFailure;

        public SaveData(int firstS, int secondS, int firstF, int secondF)
        {
            numberOfFirstTimeSucces = firstS;
            numberOfSecondTimeSucces = secondS;
            numberOfFirstTimeFailure = firstF;
            numberOfSecondTimeFailure = secondF;
        }
    }
    public class SaveLoadManager : PersistentSingleton<SaveLoadManager>
    {
        /// <summary>
        /// Save data on system
        /// </summary>
        /// <param name="firstS"></param>
        /// <param name="secondS"></param>
        /// <param name="firstF"></param>
        /// <param name="secondF"></param>
        public void SaveData(int firstS, int secondS, int firstF, int secondF)
        {
            SaveData data = LoadData();
            data.numberOfFirstTimeSucces += firstS;
            data.numberOfSecondTimeSucces += secondS;
            data.numberOfFirstTimeFailure += firstF;
            data.numberOfSecondTimeFailure += secondF;
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath
                         + "/InnovamatSaveData.dat");
            bf.Serialize(file, data);
            file.Close();
        }

        public SaveData LoadData()
        {
            if (File.Exists(Application.persistentDataPath
              + "/InnovamatSaveData.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file =
                           File.Open(Application.persistentDataPath
                           + "/InnovamatSaveData.dat", FileMode.Open);
                SaveData data = (SaveData)bf.Deserialize(file);
                file.Close();
                return data;
            }
            else
                return new SaveData(0, 0, 0, 0);
        }

        /// <summary>
        /// Resets the data on the system
        /// </summary>
        public void ResetData()
        {

            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath
                         + "/InnovamatSaveData.dat");
            SaveData data = new SaveData(0, 0, 0, 0);
            bf.Serialize(file, data);
            file.Close();

        }
    }
}