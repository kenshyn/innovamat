﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Kenshyn.Innovamat
{
    public class GameManager : PersistentSingleton<GameManager>
    {
        /// <summary>
        /// Load required level
        /// </summary>
        /// <param name="level"></param>
        public void LoadLevel(string level)
        {
            SceneManager.LoadScene(level);
        }
    }
}