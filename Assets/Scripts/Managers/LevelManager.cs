﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Kenshyn.Innovamat
{
    public class LevelManager : Singleton<LevelManager>
    {
        public int MinNumber, MaxNumber;
        public ImageAndTextFader InitialNumberText;

        public List<ImageAndTextFader> OptionsText;

        public Text InformationText;

        private int m_GeneratedNumber;
        private int m_CorrectAnswer;

        private int m_FirstTimeCorrect;
        private int m_FirstTimeWrong;
        private int m_SecondTimeCorrect;
        private int m_SecondTimeWrong;
        private bool m_FirstAnswer;
        private bool m_Busy;

        public bool Busy { get => m_Busy; }

        private void Start()
        {
            NewQuestion();
            UpdateInformationText();
        }

        private IEnumerator GameSequence()
        {
            m_Busy = true;
            // loop over 2 second forwards
            for (float i = 0; i <= 2; i += Time.deltaTime)
            {
                // set color with i as alpha
                InitialNumberText.SetTransparency(i);
                yield return null;
            }
            //wait 2 seconds
            yield return new WaitForSeconds(2);

            // loop over 2 second backwards
            for (float i = 2; i >= 0; i -= Time.deltaTime)
            {
                // set color with i as alpha
                InitialNumberText.SetTransparency(i);
                yield return null;
            }
            InitialNumberText.gameObject.SetActive(false);
            foreach (ImageAndTextFader img in OptionsText)
                img.gameObject.SetActive(true);
            // loop all other buttons
            for (float i = 0; i <= 2; i += Time.deltaTime)
            {
                // set color with i as alpha
                foreach (ImageAndTextFader img in OptionsText)
                    img.SetTransparency(i);
                yield return null;
            }
            // set color with i as alpha
            foreach (ImageAndTextFader img in OptionsText)
                img.ChangeInteractable(true);
            m_Busy = false;
        }

        private void UpdateInformationText()
        {
            InformationText.text = "Aciertos: " + (m_FirstTimeCorrect + m_SecondTimeCorrect) + "\n" + "Fallos: " + (m_FirstTimeWrong + m_SecondTimeWrong);
        }

        private void GenerateNewNumber()
        {
            InitialNumberText.gameObject.SetActive(true);

            m_FirstAnswer = true;
            m_GeneratedNumber = Random.Range(MinNumber, MaxNumber);
            InitialNumberText.SetText(NumberConverter.GetConvertedNumber((m_GeneratedNumber)));

            m_CorrectAnswer = Random.Range(0, OptionsText.Count);

            OptionsText[m_CorrectAnswer].SetText(m_GeneratedNumber.ToString());
            for (int i = 0; i < OptionsText.Count; i++)
            {
                if (i != m_CorrectAnswer)
                {
                    int randomAnswer = 0;
                    randomAnswer = Random.Range(MinNumber, MaxNumber);
                    //in case the generated number is the same as the answer or as another already generated number, we generate another random one
                    if (randomAnswer == m_GeneratedNumber && MaxNumber - MinNumber > 3)
                    {
                        while (randomAnswer == m_GeneratedNumber)
                        {
                            randomAnswer = Random.Range(MinNumber, MaxNumber);
                        }
                    }
                    OptionsText[i].SetText(randomAnswer.ToString());
                }
            }
        }

        /// <summary>
        /// Triggers next question
        /// </summary>
        private void NewQuestion()
        {
            GenerateNewNumber();

            StartCoroutine(GameSequence());
        }

        /// <summary>
        /// Saves the data and returns to the main menu
        /// </summary>
        public void EndGame()
        {
            SaveLoadManager.Instance.SaveData(m_FirstTimeCorrect, m_SecondTimeCorrect, m_FirstTimeWrong, m_SecondTimeWrong);
            GameManager.Instance.LoadLevel("MainMenu");
        }


        private IEnumerator FadeButtonsOut()
        {
            // loop over 2 second backwards
            for (float i = 2; i >= 0; i -= Time.deltaTime)
            {
                // set color with i as alpha, if the image is already faded, we let it be
                foreach (ImageAndTextFader img in OptionsText)
                    if (img.GetTransparency() > 0.01f)
                        img.SetTransparency(i);
                yield return null;
            }
        }

        /// <summary>
        /// Color the answer to either green or red
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="isGreen"></param>
        /// <returns></returns>
        private IEnumerator ColorAnswer(ImageAndTextFader answer, bool isGreen)
        {
            answer.ChangeColor(isGreen ? Color.green : Color.red);
            yield return new WaitForSeconds(0.5f);
            answer.ChangeColor(Color.white);
            yield return new WaitForSeconds(0.5f);
            answer.ChangeColor(isGreen ? Color.green : Color.red);
            yield return new WaitForSeconds(0.5f);
            answer.ChangeColor(Color.white);
            yield return new WaitForSeconds(0.5f);
            answer.ChangeColor(isGreen ? Color.green : Color.red);
        }

        /// <summary>
        /// If the answer is wrong, first we color it and then it disappears
        /// </summary>
        /// <param name="answer"></param>
        /// <returns></returns>
        private IEnumerator WrongAnswer(ImageAndTextFader answer)
        {
            yield return StartCoroutine(ColorAnswer(answer, false));

            // loop over 2 second backwards
            for (float i = 2; i >= 0; i -= Time.deltaTime)
            {
                // set color with i as alpha
                answer.SetTransparency(i);
                yield return null;
            }
            answer.ResetButton();
            m_Busy = false;
        }

        /// <summary>
        /// If the answer is right, we color it and the get a new question
        /// </summary>
        /// <param name="answer"></param>
        /// <returns></returns>
        private IEnumerator CorrectAnswer(ImageAndTextFader answer)
        {
            yield return StartCoroutine(ColorAnswer(answer, true));
            yield return StartCoroutine(FadeButtonsOut());

            answer.ResetButton();

            NewQuestion();
        }

        /// <summary>
        /// Second time the user presses the wrong answer
        /// </summary>
        /// <param name="answer"></param>
        /// <returns></returns>
        private IEnumerator SecondTimeWrongAnswer(ImageAndTextFader answer)
        {
            yield return StartCoroutine(ColorAnswer(answer, false));
            yield return StartCoroutine(CorrectAnswer(OptionsText[m_CorrectAnswer]));

            answer.ResetButton();

        }

        /// <summary>
        /// A button has been pressed, is it correct?
        /// </summary>
        /// <param name="buttonNumber"></param>
        public void ButtonPressed(int buttonNumber)
        {
            //if an answer has already been pressed, we need to wait until the animations are finished before being able to press another button
            if (m_Busy)
                return;
            m_Busy = true;
            OptionsText[buttonNumber].ChangeInteractable(false);

            if (buttonNumber == m_CorrectAnswer)
            {
                if (m_FirstAnswer)
                    m_FirstTimeCorrect++;
                else
                    m_SecondTimeCorrect++;
                StartCoroutine(CorrectAnswer(OptionsText[buttonNumber]));
            }
            else
            {
                if (m_FirstAnswer)
                {
                    m_FirstTimeWrong++;
                    m_FirstAnswer = false;
                    StartCoroutine(WrongAnswer(OptionsText[buttonNumber]));
                    //show wrong answer in red
                }
                else
                {
                    m_SecondTimeWrong++;
                    StartCoroutine(SecondTimeWrongAnswer(OptionsText[buttonNumber]));
                    //show wrong answer in red
                    //Show correct answer in green

                }
            }
            UpdateInformationText();
        }
    }
}