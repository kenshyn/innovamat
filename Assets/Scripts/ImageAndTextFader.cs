﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Kenshyn.Innovamat
{
    /// <summary>
    /// this class is used to clean up some of the code for the level manager
    /// </summary>
    public class ImageAndTextFader : MonoBehaviour
    {
        private Image m_Image;
        private Text m_Text;
        private Button m_Button;

        private bool m_Growing;

        private void Awake()
        {
            m_Image = GetComponent<Image>();
            m_Text = GetComponentInChildren<Text>();
            m_Button = GetComponent<Button>();
        }

        private void Update()
        {
            if (m_Button != null && m_Button.interactable && !LevelManager.Instance.Busy)
            {
                transform.localScale = new Vector2(transform.localScale.x + (m_Growing ? 0.001f : -0.001f), transform.localScale.y + (m_Growing ? 0.001f : -0.001f));

                if (transform.localScale.x >= 1.1f || transform.localScale.x <= 0.9f)
                    m_Growing = !m_Growing;
            }
        }

        /// <summary>
        /// sets the transparency for both the image and the text of this button
        /// </summary>
        /// <param name="alpha"></param>
        public void SetTransparency(float alpha)
        {
            m_Image.color = new Color(m_Image.color.r, m_Image.color.g, m_Image.color.b, alpha);
            m_Text.color = new Color(m_Text.color.r, m_Text.color.g, m_Text.color.b, alpha);
        }

        /// <summary>
        /// Returns the transparency of the image
        /// </summary>
        /// <returns></returns>
        public float GetTransparency()
        {
            return m_Image.color.a;
        }

        public void ChangeColor(Color c)
        {
            m_Image.color = c;
        }

        public void ChangeInteractable(bool interact)
        {
            m_Button.interactable = interact;
        }

        public void ResetButton()
        {
            m_Image.color = new Color(1, 1, 1, m_Image.color.a);
            m_Text.color = new Color(0, 0, 0, m_Text.color.a);
            transform.localScale = Vector3.one;
            m_Growing = false;

        }

        /// <summary>
        /// Sets the text associated to this button
        /// </summary>
        /// <param name="text"></param>
        public void SetText(string text)
        {
            m_Text.text = text;
        }
    }
}