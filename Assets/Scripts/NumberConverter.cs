﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Kenshyn.Innovamat
{
    static class NumberConverter
    {
        private static string[] _ones =
       {
            "cero",
            "un",
            "dos",
            "tres",
            "cuatro",
            "cinco",
            "seis",
            "siete",
            "ocho",
            "nueve"
        };

        private static string[] _teens =
        {
            "diez",
            "once",
            "doce",
            "trece",
            "catorce",
            "quince",
            "dieciseis",
            "diecisiete",
            "dieciocho",
            "diecinueve"
        };

        private static string[] _tens =
        {
            "",
            "diez",
            "veinte",
            "treinta",
            "cuarenta",
            "cincuenta",
            "sesenta",
            "setenta",
            "ochenta",
            "noventa"
        };

        // EU Nnumbering:
        private static string[] _thousandsSingular =
        {
            "",
            "mil",
            "millon",
            "mil",
            "billon",
            "mil",
            "trillon",
            "mil"
        };

        // EU Nnumbering:
        private static string[] _thousandsPlural =
        {
            "",
            "mil",
            "millones",
            "mil",
            "billones",
            "mil",
            "trillones",
            "mil"
        };

        public static string GetConvertedNumber(float value)
        {
            string digits, temp;
            bool showThousands = false;

            // Use StringBuilder to build result
            StringBuilder builder = new StringBuilder();
            // Convert integer portion of value to string
            digits = ((long)value).ToString();
            // Traverse characters in reverse order
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                // Determine if ones, tens, or hundreds column
                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            // First digit in number (last in loop)
                            //1 is a special number
                            if (ndigit == 1)
                                temp = String.Format("uno");
                            else
                                temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            // This digit is part of "teen" value
                            temp = String.Format("{0} ", _teens[ndigit]);
                            // Skip tens position
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            // Any non-zero digit
                            // 1 is a special number
                            if (ndigit == 1 && column == 0)
                                temp = String.Format("uno");
                            else
                                temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            // This digit is zero. If digit in tens and hundreds
                            // column are also zero, don't show "thousands"
                            temp = String.Empty;
                            // Test for non-zero digit in this grouping
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        // Show "thousands" if non-zero in grouping
                        if (showThousands)
                        {
                            if (column > 0)
                            {

                                if (column % 6 == 3 && column != 3)
                                {
                                    //we use this to write words even if they are all zeros
                                    if (digits[i + 1] == '0' && digits[i + 2] == '0' && digits[i + 3] == '0')
                                        temp = String.Format("{0}{1}{2}{3}{4}",
                                         ndigit == 1 ? "" : temp,
                                         ndigit == 1 && i <= 1 &&
                                         (i == 0 || digits[i - 1] == '0') &&
                                        (i <= 1 || (digits[i - 2] == '0')) &&
                                        (i <= 2 || (digits[i - 3] == '0')) &&
                                        (i <= 3 || (digits[i - 4] == '0')) &&
                                        (i <= 4 || (digits[i - 5] == '0')) ?
                                         _thousandsSingular[column / 3] : _thousandsPlural[column / 3], " ", _thousandsPlural[(column / 3) - 1],
                                        " ");
                                    else
                                        temp = String.Format("{0}{1}{2}",
                                        ndigit == 1 ? "" : temp,
                                        ndigit == 1 && i <= 1 &&
                                        (i == 0 || digits[i - 1] == '0') &&
                                        (i <= 1 || (digits[i - 2] == '0')) &&
                                        (i <= 2 || (digits[i - 3] == '0')) &&
                                        (i <= 3 || (digits[i - 4] == '0')) &&
                                        (i <= 4 || (digits[i - 5] == '0')) ?
                                        _thousandsSingular[column / 3] : _thousandsPlural[column / 3],
                                        " ");
                                    //need to check if we have to use the singular or the plural
                                }
                                else
                                {
                                    temp = String.Format("{0}{1}{2}",
                                        temp,
                                        ndigit == 1 &&
                                        (i == 0 || digits[i - 1] == '0') &&
                                         (i <= 1 || (digits[i - 2] == '0')) &&
                                         (i <= 2 || (digits[i - 3] == '0')) &&
                                        (i <= 3 || (digits[i - 4] == '0')) &&
                                        (i <= 4 || (digits[i - 5] == '0')) ?
                                        _thousandsSingular[column / 3] : _thousandsPlural[column / 3],
                                        " ");
                                }
                            }
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            // need to separate if the number is a 2
                            if (ndigit == 2 && digits[i + 1] != '0')
                                temp = String.Format("veinti");
                            else
                                temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " y " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            // need to separate if the number is a 1, and if both tens and ones are zero
                            if (ndigit == 1)
                            {
                                if (digits[i + 1] == '0' && digits[i + 2] == '0')
                                    temp = String.Format("cien ");
                                else
                                    temp = String.Format("ciento ");
                            }
                            // 5 is also a special case
                            else if (ndigit == 5)
                                temp = String.Format("quinientos ");
                            //and so is 7
                            else if (ndigit == 7)
                                temp = String.Format("setecientos ");
                            //there are almost more exceptions than normals at this point
                            else if (ndigit == 9)
                                temp = String.Format("novecientos ");
                            else
                                temp = String.Format("{0}cientos ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            // Capitalize first letter
            return String.Format("{0}{1}",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }

    }
}